# Windows_XP_NT_Theme

![thumbnail.png](./metacity-1/thumbnail.png?raw=true "Thumbnail")

## About

Windows XP NT Theme for Linux Mint Cinnamon. A fork of 'Windows XP Embedded', from 'Windows XP', by Elbullazul and  B00merang-Project. Modified menu, startbar, titlebar, frame to look more NT-like, but with XP embedded coloring. More specifically,

- lightened titlebar/border scheme
- made titlebar title etched
- made panels in titlebar, startbar flat (instead of curved)
- made title-bar button surfaces flat (instead of curved with glass/specular highlights)
- reduced frame width from from 6px to 4px (previously doubled width of frame)
- removed rounded upper corners on frame
- reduced height of titlebar, text, buttons.
- added beveled highlights / shadows to buttons, frames, some panels.

Modified cinnamon and metacity-1 folders.
Modified gtk-3.20 folder with metacity1 images and edited gtk.css for Mint 21 compatibility (temporary fixes with reduced quality)
No changes currently made to controls.
No changes made to gnome-shell, unity, xfwm4, or older / newer gtk folders content (except thumbmails).

## Setup

Place content in its own folder named 'Windows_XP_NT_Theme' in the .themes directory under ~ (home/username).

### Mint/Cinnamon <=20.3

Select name in Cinnamon System Settings, Themes, 1st/3rd/5th buttons.

### Mint/Cinnamon >=21.0

Select name in Cinnamon System Settings, Themes, 2nd/4th buttons.

![thumbnail.png](./cinnamon/thumbnail.png?raw=true "Screenshot")
